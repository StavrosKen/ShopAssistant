# Agrimetrics Shop Assistant Library Test
A JS library that takes orders from customers, sequences the delivery and outputs the schedule.

## Installation:
  - Install Node.js
  - From a terminal, navigate inside the project root folder
  - Run ``` npm install ```

## Usage
  - Run ``` npm start ``` to run the example usage script of the library
  - Run ``` npm test ``` to run the unit tests
  
 ## API

Create a new ShopAssistant providing an options object. For example: 
``` {
    steps: [
        {name: 'make', time: 60 },
        {name: 'serve', time: 30 }
    ],
    subject: 'sandwich'
}
```

To add a new order call ```addOrder()``` providing an optional options object.
To output the schedule call ```showSchedule()```.
The shopAssistant also exposes the raw ```orders``` and ```schedule``` arrays in case they need to be accessed.
