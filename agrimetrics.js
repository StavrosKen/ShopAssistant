var ShopAssistant = require('./shopAssistant');

var options = {
    steps: [
        {name: 'make', time: 60 },
        {name: 'serve', time: 30 }
    ],
    subject: 'sandwich'
};

var SandwichShop = new ShopAssistant(options);

SandwichShop.addOrder({ingredients: 'beef'});
SandwichShop.addOrder({ingredients: 'pork'});
SandwichShop.addOrder({ingredients: 'pork'});

SandwichShop.showSchedule();