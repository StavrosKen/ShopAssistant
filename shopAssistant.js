var shopAssistant = function (options) {
 
    var steps   = options.steps || [ {name: 'make', time: 60 }, {name: 'serve', time: 30 }],
        subject = options.subject || 'sandwich',
        orders  = [],
        schedule = [],
        totalTimePerOrder = 0;

    steps.forEach(function(step) {
        totalTimePerOrder = totalTimePerOrder + step.time;
    });

    (function init() {
        console.log('Created new '+ subject +' shop with delivery steps: \x1b[36m', steps);
        console.log('\x1b[0m');
    })();

    function addOrder(newOrder) {
        newOrder.dateCreated = new Date();
        newOrder.dateScheduled = scheduleOrder(newOrder.dateCreated);
        orders.push(newOrder);
        orderTasksScheduler(newOrder);
    }

    function scheduleOrder(orderDateCreated) {
        if (orders.length < 1) {
            return orderDateCreated;
        }
        else {
            var lastScheduledOrderTime = orders[orders.length - 1].dateScheduled.getTime(),
                secondsDifference = orderDateCreated.getTime() - lastScheduledOrderTime;
            if (secondsDifference < totalTimePerOrder * 1000) {
                return new Date(lastScheduledOrderTime + totalTimePerOrder * 1000);
            }
            else {
                return orderDateCreated;
            }
        }
    }

    function orderTasksScheduler(order) {
        var nextStepDelay = 0;
        steps.forEach(function (step) {
            schedule.push({orderId: orders.length, stepName: step.name, dateScheduled: delayTask(order.dateScheduled, nextStepDelay)});
            nextStepDelay = step.time;
        })
    }

    function delayTask(start, delay) {
        return new Date(start.getTime() + delay * 1000);
    }

    function showSchedule() {
        console.log(orders.length + ' ' + subject + ' orders placed \n');
        for (var i = 0; i < schedule.length; i++) {
            console.log((i+1) + '. ' + dateFormater(schedule[i].dateScheduled) + ' ' + schedule[i].stepName + ' ' + subject + ' ' + schedule[i].orderId);
        }
    }

    function dateFormater(date) {
        return date.getHours() +':'+ date.getMinutes() +':'+ date.getSeconds();
    }


    return {
        addOrder: addOrder,
        showSchedule: showSchedule,
        schedule: schedule,
        orders: orders
    };

};

module.exports = shopAssistant;