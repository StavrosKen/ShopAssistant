describe("Shop Assistant", function() {
    var shopAssistant = require('../shopAssistant');
    var options = {
        steps: [
            {name: 'make', time: 60 },
            {name: 'serve', time: 30 }
        ],
        subject: 'sandwich'
    },
    SandwichShop;

    beforeEach(function() {
        SandwichShop = new shopAssistant(options);
    });
  
    it("should initially have no orders", function() {
      expect(SandwichShop.orders.length).toEqual(0);
    });

    function testShopForOrders(ordersCount) {
        describe("adding "+ ordersCount +" orders", function() {
            beforeEach(function() {
                for(var i = 0; i < ordersCount; i++) {
                    SandwichShop.addOrder({ingredients: 'beef'});
                }
            });
            it("should result in the shop having 1 order", function() {
                expect(SandwichShop.orders.length).toEqual(ordersCount);
            });
            it("should make the schedule have the correct length", function() {
                var shopSchedule = SandwichShop.schedule;
                expect(shopSchedule.length).toEqual(options.steps.length * ordersCount);
            });
            it("should schedule its tasks according to the steps duration ", function() {
                var shopSchedule = SandwichShop.schedule;
                for(var i = 0; i < shopSchedule.length - 1; i++) {
                    expect(shopSchedule[i + 1].dateScheduled.getTime() - shopSchedule[i].dateScheduled.getTime()).toEqual(options.steps[i%options.steps.length].time * 1000);
                }
            });
        });
    }

    testShopForOrders(3);

  });
  
